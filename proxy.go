package migrator

import (
	"context"
	"database/sql"
	"fmt"
	"io/ioutil"
	"net/http"

	proxysql "github.com/GoogleCloudPlatform/cloudsql-proxy/proxy/dialers/mysql"
	"github.com/GoogleCloudPlatform/cloudsql-proxy/proxy/proxy"
	"github.com/go-sql-driver/mysql"
	goauth "golang.org/x/oauth2/google"
)

// mustOpen connects to a database and panics on error.
func mustOpen(cfg *Config) *sql.DB {
	db, err := open(cfg)
	if err != nil {
		panic(err)
	}

	return db
}

// open opens up a SQL connection to a Cloud SQL Instance specified by the
// provided configuration.
func open(cfg *Config) (*sql.DB, error) {
	//if token/tokenFile are not set, the github.com/GoogleCloudPlatform/cloudsql-proxy/proxy/dialers/mysql library
	//will automatically attempt to find default authentication via proxy.InitDefault */
	if cfg.TokenFile != "" {
		client, err := clientFromCredentials(cfg.TokenFile)
		if err != nil {
			return nil, err
		}

		proxy.Init(client, nil, nil)
	}

	var db *sql.DB
	var err error
	fmt.Printf("connecting with %s to db: %s(%s)\n", cfg.User, cfg.InstanceName, cfg.DBName)
	db, err = proxysql.DialCfg(&mysql.Config{
		User:                 cfg.User,
		Addr:                 cfg.InstanceName,
		DBName:               cfg.DBName,
		Passwd:               cfg.Passwd,
		MultiStatements:      true,
		AllowNativePasswords: true,
	})

	return db, err
}

func clientFromCredentials(tokenFile string) (*http.Client, error) {
	const SQLScope = "https://www.googleapis.com/auth/sqlservice.admin"
	ctx := context.Background()

	var client *http.Client
	if f := tokenFile; f != "" {
		all, err := ioutil.ReadFile(f)
		if err != nil {
			return nil, fmt.Errorf("invalid json file %q: %v", f, err)
		}
		cfg, err := goauth.JWTConfigFromJSON(all, SQLScope)
		if err != nil {
			return nil, fmt.Errorf("invalid json file %q: %v", f, err)
		}
		client = cfg.Client(ctx)
	}

	return client, nil
}
