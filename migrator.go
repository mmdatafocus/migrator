package migrator

import (
	"database/sql"
	"fmt"
	"strings"

	"github.com/mattes/migrate"
	msql "github.com/mattes/migrate/database/mysql"
	// need to import for file driver
	_ "github.com/mattes/migrate/source/file"
)

// Config is a configuration parsed to migrate for either local db or Google Cloud SQL
// It can have either ConnStr for local db or the rest fields for Google Cloud SQL
// NOTE: it only works for MySQL
type Config struct {
	ConnStr      string
	TokenFile    string
	InstanceName string
	User         string
	Passwd       string
	DBName       string
}

// Migrate will migrate database
func Migrate(cfg *Config) error {
	err := createDatabaseIfNotExists(cfg)
	if err != nil {
		panic(err)
	}

	db, err := getDatabase(cfg)
	defer db.Close()

	driver, err := msql.WithInstance(db, &msql.Config{})
	if err != nil {
		panic(err)
	}

	m, err := migrate.NewWithDatabaseInstance("file://files", "mysql", driver)
	if err != nil {
		panic(err)
	}

	err = m.Up()
	if err != nil && err != migrate.ErrNoChange {
		return err
	}

	if err == migrate.ErrNoChange {
		fmt.Println("no change")
	}

	return nil
}

func createDatabaseIfNotExists(cfg *Config) error {
	var db *sql.DB
	var err error
	if len(cfg.ConnStr) > 0 {
		db, err = sql.Open("mysql", cfg.ConnStr[:strings.LastIndex(cfg.ConnStr, "/")]+"/")
	} else {
		if cfg.InstanceName == "" {
			panic("ConnStr or InstanceName must be provided.")
		}
		db, err = open(&Config{
			InstanceName: cfg.InstanceName,
			User:         cfg.User,
			Passwd:       cfg.Passwd,
			TokenFile:    cfg.TokenFile,
		})
	}

	if err != nil {
		return err
	}

	defer db.Close()

	_, err = db.Exec("CREATE DATABASE IF NOT EXISTS " + cfg.DBName)
	if err != nil {
		return err
	}

	return nil
}

func getDatabase(cfg *Config) (*sql.DB, error) {
	var db *sql.DB
	var err error
	if len(cfg.ConnStr) > 0 {
		db, err = sql.Open("mysql", cfg.ConnStr)
	} else {
		db, err = open(cfg)
	}

	return db, err
}
